import Vue from 'vue'
import App from './App'
import pageHead from './components/page-head.vue'
import store from './store'

Vue.config.productionTip = false
Vue.prototype.$store = store

App.mpType = 'app'
Vue.component('page-head', pageHead)
const app = new Vue({
	store,
    ...App
})
app.$mount()


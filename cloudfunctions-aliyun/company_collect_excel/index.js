'use strict';
const db = uniCloud.database()
const dbCmd = db.command
const diffTime = 8 * 60 * 60 * 1000;
// 获取指定的今天的时间戳
function getMsByDate(timeStr, dayStr) {
	return new Date(dayStr + ' ' + timeStr).getTime() - diffTime;
}
exports.main = async (event, context) => {
	const whereArr = {};
	if (event.day) {
		whereArr.dayTime = getMsByDate('00:00:00', event.day);
	}
	if (event.company) {
		whereArr.company = event.company;
	}
	if(JSON.stringify(whereArr) === '{}') {
		whereArr.createTime = dbCmd.gt(0)
	}

	const skipCount = (event.page - 1) * event.size;
	let collection = db.collection('company_collect').where(whereArr);

	const totalRes = await collection.count();
	let res = {};
	if (totalRes.total > 0) {
		res = await collection.skip(skipCount).limit(event.size).orderBy("company", "desc").orderBy("createTime", "desc").get();
	}
	return {
		page: event.page,
		size: event.size,
		total: totalRes.total || 0,
		data: res.data || []
	};
};

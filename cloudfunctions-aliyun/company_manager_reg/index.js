'use strict';
const db = uniCloud.database();
// 密码加密方法
var crypto = require('crypto');
const passSecret = 'dx8ygdpzcm85qcfennu0q';

function encryptPassword(password) {
	var mysecret = passSecret + password;
	return crypto.createHash('md5').update(mysecret).digest("hex")
}
exports.main = async (event, context) => {

	event.createTime = Date.now();
	event.department = '';
	event.company_no = event.username;
	event.password = encryptPassword(event.password);
	const collection = db.collection('company_accout');
	const userRes = await collection.where({
		'company_no': event.company_no
	}).get();

	if (userRes.data.length > 0) {
		return {
			status: 0,
			errorMsg: '企业已被注册'
		}
	}


	await collection.add(event);
	return {
		status: 1
	}

};

'use strict';
const db = uniCloud.database()
const dbCmd = db.command
exports.main = async (event, context) => {
	const skipCount = (event.page - 1) * event.size;
	
	let collection = db.collection('company_suggest').where({
		createTime:dbCmd.gt(0)
	});
	
	const totalRes = await collection.count();
	let res = {};
	if (totalRes.total>0) {
		res = await collection.orderBy("createTime", "desc").get();
	}
	return {
		page: event.page,
		size: event.size,
		total: totalRes.total || 0,
		data: res.data || []
	};
};
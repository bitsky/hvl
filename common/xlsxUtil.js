// #ifdef H5
var XLSX = require('xlsx');
const xlsxUtil = {
	init: function(funName, params, excel_fields, domId, fileName) {
		this.funName = funName;
		params.page = 1;
		params.size = params.size ? params.size : 100;
		this.params = params;
		this.excel_fields = excel_fields;
		this.outFile = document.getElementById(domId);
		this.fileName = fileName;
		this.arr = [];
		uni.showLoading({
			title: '处理中...'
		});
	},
	getExcelData:function() {
		uniCloud.callFunction({
			name: this.funName,
			data: this.params,
		}).then((res) => {
			var maxPage = Math.ceil(res.result.total/this.params.size);
			if (res.result.data.length>0) {
				this.arr = this.arr.concat(res.result.data);
			}
			if (this.params.page<maxPage) {
				this.params.page++;
				this.getExcelData();
			}
			else {
				uni.hideLoading()
				this.downloadFile();
			}
		}).catch((err) => {
			console.log(err);
			uni.hideLoading()
		})
	},
	downloadFile: function() { // 点击导出按钮
		
		let data = [this.excel_fields];
		data = data.concat(this.arr);
		console.log(data);
		xlsxUtil.downloadExl(data)

	},
	downloadExl: function(json) { // 导出到excel
		let keyMap = Object.keys(this.excel_fields); // 获取键
		// for (let k in json[0]) {
		// 	keyMap.push(k)
		// }
		console.info('keyMap', keyMap, json)
		let tmpdata = [] // 用来保存转换好的json
		const arr = json.map((v, i) => {
			return keyMap.map((k, j) => {
				return Object.assign({}, {
					v: typeof v[k] === 'object' ? v[k].title : v[k],
					position: (j > 25 ? this.getCharCol(j) : String.fromCharCode(65 + j)) + (i + 1),
					func: (i > 0 && typeof this.excel_fields[k] === 'object') ? this.excel_fields[k].callback : null
				});
			});
		});
		arr.reduce((prev, next) => prev.concat(next)).forEach(function(v) {
			tmpdata[v.position] = {
				v: v.func ? v.func(v.v) : (v.v || '')
			}
		});
		let outputPos = Object.keys(tmpdata) // 设置区域,比如表格从A1到D10
		let tmpWB = {
			SheetNames: ['mySheet'], // 保存的表标题
			Sheets: {
				'mySheet': Object.assign({},
					tmpdata, // 内容
					{
						'!ref': outputPos[0] + ':' + outputPos[outputPos.length - 1] // 设置填充区域
					})
			}
		}
		let tmpDown = new Blob([this.s2ab(XLSX.write(tmpWB, {
				bookType: 'xlsx',
				bookSST: false,
				type: 'binary'
			} // 这里的数据是用来定义导出的格式类型
		))], {
			type: ''
		}) // 创建二进制对象写入转换好的字节流
		var href = URL.createObjectURL(tmpDown) // 创建对象超链接
		this.outFile.download = this.fileName + '.xlsx' // 下载名称
		this.outFile.href = href // 绑定a标签
		this.outFile.click() // 模拟点击实现下载
		setTimeout(function() { // 延时释放
			URL.revokeObjectURL(tmpDown) // 用URL.revokeObjectURL()来释放这个object URL
		}, 100)
	},
	s2ab: function(s) { // 字符串转字符流
		var buf = new ArrayBuffer(s.length)
		var view = new Uint8Array(buf)
		for (var i = 0; i !== s.length; ++i) {
			view[i] = s.charCodeAt(i) & 0xFF
		}
		return buf
	},
	getCharCol: function(n) { // 将指定的自然数转换为26进制表示。映射关系：[0-25] -> [A-Z]。
		let s = ''
		let m = 0
		while (n > 0) {
			m = n % 26 + 1
			s = String.fromCharCode(m + 64) + s
			n = (n - m) / 26
		}
		return s
	}
};

module.exports = {
	xlsxUtil,
}
// #endif
